#include <stdio.h>

#define ROWS 10    /* if you need a constant, #define one (or more) */
#define COLS 4

/* fill m[ROWS][COLS] with values read from fp.
 * returns 1 on success, 0 otherwise.
 */
int getmatrix (int (*m)[COLS], FILE *fp)
{
	for (int row = 0; row < ROWS; row++) 
	{
		for (int col = 0; col < COLS; col++) 
		{
			if (fscanf (fp, "%d", &m[row][col]) != 1) 
			{
				fprintf (stderr, "error reading m[%d][%d]\n", row, col);
			return 0;
			}
		}
	}
  
	return 1;
}

void prnmatrix (int (*m)[COLS])
{
	for (int row = 0; row < ROWS; row++)
	{
		for (int col = 0; col < COLS; col++)
		{
//			printf (col ? " %4d" : "%4d", m[row][col]);
			printf(" %2d",m[row][col]);
		}
		putchar ('\n');
	}
}

void teste (int (*m)[COLS])
{
	int teste;
	teste = m[9][2] + m[9][3];
	printf("%d\n", teste);
	putchar('\n');
}


int fcfs (int (*m)[COLS])
{
	int fcfs_matrix[ROWS][COLS] = {{0}};
	int comparator;
	int index;

	for (int i = 0; i < ROWS; i++)
	{
	comparator = 1000;
		for (int row = 0; row < ROWS; row++)
		{
			if (m[row][2] > comparator);
			{
				index == row;
				comparator == m[row][2];
					
			}
			for (int col = 0; col < COLS; col++)
			{
				fcfs_matrix[index][col] = m[index][col];
			}
			
		}
		m[index][2] = 1001;
	}
	prnmatrix(fcfs_matrix);

}

int main (int argc, char **argv) {
  
	int matrix[ROWS][COLS] = {{0}};   /* do not use global variables */
	/* use filename provided as 1st argument (stdin by default) */
	//FILE *fp = argc > 1 ? fopen (argv[1], "r") : stdin;

	
	FILE *fp = fopen("entrada.txt", "rt");
	
	if (fp == NULL) {  /* validate file open for reading in caller */
	perror ("file open failed");
	return 1;
  }
  
	if (!getmatrix (matrix, fp))    /* pass array and open FILE*, validate */
	{
		return 1;
	}
	fclose (fp);
	fcfs(matrix);
//	prnmatrix (matrix);       /* output results */
}


